CREATE SCHEMA IF NOT EXISTS lab4_schema;

CREATE SEQUENCE lab4_schema.service_user_seq
    INCREMENT BY 1
    MINVALUE 1
    START WITH 1;

CREATE TABLE IF NOT EXISTS lab4_schema.service_user (
    id BIGINT NOT NULL,
    version INTEGER,
    date_created TIMESTAMP(6),
    dob TIMESTAMP(6),
    email VARCHAR(255) NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255),
    nickname VARCHAR(255),
    phone_number VARCHAR(255) NOT NULL,
    sex VARCHAR(255) NOT NULL CHECK (sex IN ('MALE','FEMALE')),

    PRIMARY KEY (id),
    CONSTRAINT email_idx UNIQUE (email),
    CONSTRAINT phone_idx UNIQUE (phone_number)
)