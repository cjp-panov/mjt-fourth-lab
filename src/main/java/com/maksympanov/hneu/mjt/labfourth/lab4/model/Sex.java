package com.maksympanov.hneu.mjt.labfourth.lab4.model;

public enum Sex {
    MALE,
    FEMALE;

    public static String stringValue(Sex sex) {
        if (sex == MALE) {
            return "Male";
        }
        return "Female";
    }

}
