package com.maksympanov.hneu.mjt.labfourth.lab4.controller;

import com.maksympanov.hneu.mjt.labfourth.lab4.dto.UserUpdateDto;
import com.maksympanov.hneu.mjt.labfourth.lab4.service.ServiceUserLogic;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;

@WebServlet("/users/*")
@ApplicationScoped
public class ServiceUserController extends HttpServlet {

    @Inject
    private ServiceUserLogic serviceUserLogic;

    @Inject
    private DataMapper dataMapper;

    @Override
    @SneakyThrows
    public void doGet(HttpServletRequest req, HttpServletResponse res) {
        var pathInfo = req.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            var users = serviceUserLogic.getListOfAllUsersInSystem();
            var usersDto = dataMapper.mapUsersInfoDto(users);
            req.setAttribute("usersDto", usersDto);
            req.getRequestDispatcher("/WEB-INF/pages/users-list.jsp")
                    .forward(req, res);
        } else {
            var idStr = StringUtils.substringAfterLast(pathInfo, "/");
            long id;
            try {
                id = Long.parseLong(idStr);
            } catch (Exception e) {
                req.setAttribute("errorMessage", "Provided ID '%s' is not a valid identifier".formatted(idStr));
                req.getRequestDispatcher("/WEB-INF/pages/error-page.jsp")
                        .forward(req, res);
                return;
            }

            if (!serviceUserLogic.existsById(id)) {
                req.setAttribute("errorMessage", "User with ID '%s' does not exist".formatted(idStr));
                req.getRequestDispatcher("/WEB-INF/pages/error-page.jsp")
                        .forward(req, res);
                return;
            }

            var user = serviceUserLogic.getServiceUserByIdThrowable(id);
            var userDto = dataMapper.mapUserInfoDto(user);
            req.setAttribute("user", userDto);

            req.getRequestDispatcher("/WEB-INF/pages/user-info.jsp")
                    .forward(req, res);
        }
    }

    @Override
    @SneakyThrows
    public void doPost(HttpServletRequest req, HttpServletResponse res) {
        var pathInfo = req.getPathInfo();

        if (pathInfo != null && pathInfo.startsWith("/delete")) {
            var id = getIdFromRequest(req, res, pathInfo);
            if (id == -1) {
                return;
            }
            try {
                serviceUserLogic.deleteServiceUser(id);
                res.sendRedirect("/result/success");
            } catch (Exception e) {
                req.setAttribute("errorMessage", "User with ID '%s' does not exist".formatted(id));
                req.getRequestDispatcher("/WEB-INF/pages/error-page.jsp")
                        .forward(req, res);
            }
            return;
        }

        if (pathInfo != null && !pathInfo.equals("/")) {
            var id = getIdFromRequest(req, res, pathInfo);
            if (id == -1) {
                return;
            }
            var dto = UserUpdateDto.builder()
                    .firstName(req.getParameter("firstName"))
                    .lastName(req.getParameter("lastName"))
                    .nickname(req.getParameter("nickname"))
                    .email(req.getParameter("email"))
                    .phoneNumber(req.getParameter("phoneNumber"))
                    .dateOfBirth(req.getParameter("dob"))
                    .build();

            var user = serviceUserLogic.getServiceUserByIdNullable(id);

            var email = dto.getEmail();
            if (user != null && !StringUtils.equals(user.getEmail(), email) && serviceUserLogic.existsByUniqueField(email)) {
                req.setAttribute("errorMessage", "User with email %s already exists".formatted(email));
                req.getRequestDispatcher("/WEB-INF/pages/error-page.jsp")
                        .forward(req, res);
                return;
            }

            var phoneNumber = dto.getPhoneNumber();
            if (user != null && !StringUtils.equals(user.getPhoneNumber(), phoneNumber) && serviceUserLogic.existsByUniqueField(phoneNumber)) {
                req.setAttribute("errorMessage", "User with phone number %s already exists".formatted(phoneNumber));
                req.getRequestDispatcher("/WEB-INF/pages/error-page.jsp")
                        .forward(req, res);
                return;
            }

            serviceUserLogic.updateUser(id, dto);

            res.sendRedirect("/result/success");
        }
    }

    @SneakyThrows
    private long getIdFromRequest(HttpServletRequest req, HttpServletResponse res, String pathInfo) {
        var idStr = StringUtils.substringAfterLast(pathInfo, "/");
        long id;
        try {
            id = Long.parseLong(idStr);
        } catch (Exception e) {
            req.setAttribute("errorMessage", "Provided ID '%s' is not a valid identifier".formatted(idStr));
            req.getRequestDispatcher("/WEB-INF/pages/error-page.jsp")
                    .forward(req, res);
            return -1;
        }

        if (!serviceUserLogic.existsById(id)) {
            req.setAttribute("errorMessage", "User with ID '%s' does not exist".formatted(idStr));
            req.getRequestDispatcher("/WEB-INF/pages/error-page.jsp")
                    .forward(req, res);
            return -1;
        }

        return id;
    }

}