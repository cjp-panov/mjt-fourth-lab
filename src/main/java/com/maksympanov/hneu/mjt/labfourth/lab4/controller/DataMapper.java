package com.maksympanov.hneu.mjt.labfourth.lab4.controller;

import com.maksympanov.hneu.mjt.labfourth.lab4.dto.UserInfoDto;
import com.maksympanov.hneu.mjt.labfourth.lab4.dto.UsersInfoDto;
import com.maksympanov.hneu.mjt.labfourth.lab4.model.ServiceUser;
import com.maksympanov.hneu.mjt.labfourth.lab4.model.Sex;
import jakarta.enterprise.context.ApplicationScoped;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

@ApplicationScoped
public class DataMapper {

    public UserInfoDto mapUserInfoDto(ServiceUser user) {
        return UserInfoDto.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .nickname(user.getNickname())
                .email(user.getEmail())
                .phoneNumber(user.getPhoneNumber())
                .sex(Sex.stringValue(user.getSex()))
                .dateOfBirth(user.getDateOfBirth())
                .dateAdded(user.getDateCreated()
                        .format(
                            DateTimeFormatter.ofPattern(
                                "dd MMMM yyyy HH:mm:ss",
                                new Locale("uk", "UA")
                            )
                        )
                )
                .build();
    }

    public UsersInfoDto mapUsersInfoDto(List<ServiceUser> users) {
        var rv = users.stream()
                .map(this::mapUserInfoDto)
                .toList();

        return UsersInfoDto.builder()
                .users(rv)
                .usersCount(rv.size())
                .build();
    }

}
