package com.maksympanov.hneu.mjt.labfourth.lab4.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserUpdateDto {

    private String firstName;

    private String lastName;

    private String nickname;

    private String email;

    private String phoneNumber;

    private String dateOfBirth;

}
