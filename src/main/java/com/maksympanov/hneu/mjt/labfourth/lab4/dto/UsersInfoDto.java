package com.maksympanov.hneu.mjt.labfourth.lab4.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UsersInfoDto {

    private List<UserInfoDto> users;

    private Integer usersCount;

}
