package com.maksympanov.hneu.mjt.labfourth.lab4.dao;

import jakarta.annotation.PreDestroy;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.*;

@ApplicationScoped
public class ConnectionProvider {

    private final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CRM");

    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @PreDestroy
    public void closeDataSource() {
        if (entityManagerFactory != null) {
            entityManagerFactory.close();
        }
    }

}
