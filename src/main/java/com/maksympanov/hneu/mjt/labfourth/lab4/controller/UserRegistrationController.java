package com.maksympanov.hneu.mjt.labfourth.lab4.controller;

import com.maksympanov.hneu.mjt.labfourth.lab4.dto.UserRegistrationDto;
import com.maksympanov.hneu.mjt.labfourth.lab4.service.ServiceUserLogic;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;

@ApplicationScoped
@WebServlet("/registration")
public class UserRegistrationController extends HttpServlet {

    @Inject
    private ServiceUserLogic serviceUserLogic;

    @Override
    @SneakyThrows
    public void doGet(HttpServletRequest req, HttpServletResponse res) {
        req.getRequestDispatcher("/WEB-INF/pages/create-user.jsp").forward(req, res);
    }

    @Override
    @SneakyThrows
    public void doPost(HttpServletRequest req, HttpServletResponse res) {
        var dto = UserRegistrationDto.builder()
                .firstName(req.getParameter("firstName"))
                .lastName(req.getParameter("lastName"))
                .nickname(req.getParameter("nickname"))
                .email(req.getParameter("email"))
                .phoneNumber(req.getParameter("phoneNumber"))
                .sex(req.getParameter("sex"))
                .dateOfBirth(req.getParameter("dob"))
                .build();

        var phoneNumber = dto.getPhoneNumber();
        if (serviceUserLogic.existsByUniqueField(phoneNumber)) {
            req.setAttribute("errorMessage", "User with phone number %s already exists".formatted(phoneNumber));
            req.getRequestDispatcher("/WEB-INF/pages/error-page.jsp")
                    .forward(req, res);
            return;
        }

        var email = dto.getEmail();
        if (serviceUserLogic.existsByUniqueField(email)) {
            req.setAttribute("errorMessage", "User with email %s already exists".formatted(email));
            req.getRequestDispatcher("/WEB-INF/pages/error-page.jsp")
                    .forward(req, res);
            return;
        }

        serviceUserLogic.createNewUser(dto);

        res.sendRedirect("/result/success");
    }

}
