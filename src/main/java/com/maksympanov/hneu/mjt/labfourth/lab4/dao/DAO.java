package com.maksympanov.hneu.mjt.labfourth.lab4.dao;

import java.util.List;
import java.util.Optional;

public interface DAO<T, ID> {

    boolean existsById(ID id);

    boolean existsByUniqueStringField(String fieldValue);

    Optional<T> findById(ID id);

    List<T> findAll();

    T save(T entity);

    void delete(T entity);

}
