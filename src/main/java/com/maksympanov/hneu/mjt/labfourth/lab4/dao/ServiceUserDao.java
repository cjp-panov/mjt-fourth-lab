package com.maksympanov.hneu.mjt.labfourth.lab4.dao;

import com.maksympanov.hneu.mjt.labfourth.lab4.model.ServiceUser;
import jakarta.annotation.PreDestroy;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Optional;

@RequestScoped
public class ServiceUserDao implements DAO<ServiceUser, Long> {

    private final EntityManager em;

    @Inject
    public ServiceUserDao(ConnectionProvider connectionProvider) {
        em = connectionProvider.getEntityManager();
        System.out.printf("Initialized EntityManager (%s) for request, thread - %s\n", em, Thread.currentThread().getName());
    }

    @PreDestroy
    public void teardown() {
        em.close();
        System.out.printf("Closed EntityManager (%s) after request end, thread - %s\n", em, Thread.currentThread().getName());
    }


    @Override
    public Optional<ServiceUser> findById(Long id) {
        var found = em.find(ServiceUser.class, id);
        return Optional.ofNullable(found);
    }

    @Override
    public List<ServiceUser> findAll() {
        return em.createQuery("from ServiceUser", ServiceUser.class)
                .getResultList();
    }

    @Override
    public ServiceUser save(ServiceUser userToSave) {
        var id = userToSave.getId();

        if (existsById(id)) {
            var userInDB = findById(id).get();
            em.getTransaction().begin();
            updateUserWithNewValues(userInDB, userToSave, em);
            em.getTransaction().commit();
            return userInDB;
        }

        userToSave.setId(null);
        em.getTransaction().begin();
        em.persist(userToSave);
        em.getTransaction().commit();
        return userToSave;
    }

    @Override
    public void delete(ServiceUser entity) {
        em.getTransaction().begin();
        em.createQuery("delete from ServiceUser where id=:id")
                .setParameter("id", entity.getId())
                .executeUpdate();
        em.getTransaction().commit();
    }

    @Override
    public boolean existsById(Long id) {
        var count = (Long) em.createQuery("select count(*) from ServiceUser where id=:id")
                .setParameter("id", id)
                .getSingleResult();
        return count > 0;
    }

    @Override
    public boolean existsByUniqueStringField(String fieldValue) {
        var count = (Long) em.createQuery("select count(*) from ServiceUser where email=:val or phoneNumber=:val")
                .setParameter("val", fieldValue)
                .getSingleResult();
        return count > 0;
    }

    private void updateUserWithNewValues(ServiceUser userInDB, ServiceUser userToSave, EntityManager em) {
        var firstName = userToSave.getFirstName();
        if (!StringUtils.isEmpty(firstName)) {
            userInDB.setFirstName(firstName);
        }

        var lastName = userToSave.getLastName();
        if (!StringUtils.isEmpty(lastName)) {
            userInDB.setLastName(lastName);
        }

        var nickname = userToSave.getNickname();
        if (!StringUtils.isEmpty(nickname)) {
            userInDB.setNickname(nickname);
        }

        var email = userToSave.getEmail();
        if (!StringUtils.isEmpty(email)) {
            userInDB.setEmail(email);
        }

        var phoneNumber = userToSave.getPhoneNumber();
        if (!StringUtils.isEmpty(phoneNumber)) {
            userInDB.setPhoneNumber(phoneNumber);
        }

        var sex = userToSave.getSex();
        if (sex != null) {
            userInDB.setSex(sex);
        }

        var dob = userToSave.getDateOfBirth();
        if (dob != null) {
            userInDB.setDateOfBirth(dob);
        }
    }

}
