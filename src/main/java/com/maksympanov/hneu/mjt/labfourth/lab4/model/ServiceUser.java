package com.maksympanov.hneu.mjt.labfourth.lab4.model;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@ToString
@Entity(name = "ServiceUser")
@Table(
        name = "service_user",
        schema = "lab4_schema",
        uniqueConstraints = {
                @UniqueConstraint(name = "unique_email", columnNames = "email"),
                @UniqueConstraint(name = "unique_phone", columnNames = "phone_number")
        },
        indexes = {
                @Index(name = "email_idx", columnList = "email", unique = true),
                @Index(name = "phone_idx", columnList = "phone_number", unique = true),
        }
)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ServiceUser {

    @Id
    @EqualsAndHashCode.Include
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "ServiceUserIdSequenceGenerator"
    )
    @SequenceGenerator(
            name = "ServiceUserIdSequenceGenerator",
            sequenceName = "service_user_seq",
            schema = "lab4_schema",
            allocationSize = 1
    )
    private Long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    private String nickname;

    @Column(nullable = false)
    private String email;

    @Column(name = "phone_number", nullable = false)
    private String phoneNumber;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @Column(name = "dob")
    private LocalDate dateOfBirth;

    @Version
    private Integer version;

    @Column(name = "date_created")
    @CreationTimestamp
    private LocalDateTime dateCreated;

}
