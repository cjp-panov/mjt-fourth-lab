package com.maksympanov.hneu.mjt.labfourth.lab4.service;

import com.maksympanov.hneu.mjt.labfourth.lab4.dao.DAO;
import com.maksympanov.hneu.mjt.labfourth.lab4.dto.UserRegistrationDto;
import com.maksympanov.hneu.mjt.labfourth.lab4.dto.UserUpdateDto;
import com.maksympanov.hneu.mjt.labfourth.lab4.model.ServiceUser;
import com.maksympanov.hneu.mjt.labfourth.lab4.model.Sex;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityNotFoundException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;


@ApplicationScoped
public class ServiceUserLogic {

    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Inject
    private DAO<ServiceUser, Long> serviceUserDao;

    public boolean existsById(Long id) {
        return serviceUserDao.existsById(id);
    }

    public boolean existsByUniqueField(String fieldValue) {
        return serviceUserDao.existsByUniqueStringField(fieldValue);
    }

    public List<ServiceUser> getListOfAllUsersInSystem() {
        return serviceUserDao.findAll();
    }

    public ServiceUser getServiceUserByIdThrowable(Long id) {
        return serviceUserDao.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Could not find ServiceUser with id: " + id));
    }

    public ServiceUser getServiceUserByIdNullable(Long id) {
        return serviceUserDao.findById(id)
                .orElse(null);
    }

    public ServiceUser createNewUser(UserRegistrationDto dto) {
        var dateOfBirth = LocalDate.parse(dto.getDateOfBirth(), FORMATTER);
        var sex = Sex.valueOf(dto.getSex());

        return serviceUserDao.save(
                ServiceUser.builder()
                        .firstName(dto.getFirstName())
                        .lastName(dto.getLastName())
                        .nickname(dto.getNickname())
                        .email(dto.getEmail())
                        .phoneNumber(dto.getPhoneNumber())
                        .sex(sex)
                        .dateOfBirth(dateOfBirth)
                        .build()
        );
    }

    // TODO: validation
    public void updateUser(Long userId, UserUpdateDto dto) {
        var dateOfBirth = LocalDate.parse(dto.getDateOfBirth(), FORMATTER);

        serviceUserDao.save(
                ServiceUser.builder()
                        .id(userId)
                        .firstName(dto.getFirstName())
                        .lastName(dto.getLastName())
                        .nickname(dto.getNickname())
                        .email(dto.getEmail())
                        .phoneNumber(dto.getPhoneNumber())
                        .dateOfBirth(dateOfBirth)
                        .build()
        );
    }

    public void deleteServiceUser(Long id) {
        var user = serviceUserDao.findById(id);
        user.ifPresent(serviceUserDao::delete);
    }

}
