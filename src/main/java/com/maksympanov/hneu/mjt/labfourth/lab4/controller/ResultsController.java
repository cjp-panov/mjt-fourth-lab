package com.maksympanov.hneu.mjt.labfourth.lab4.controller;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;

@WebServlet("/result/*")
public class ResultsController extends HttpServlet {

    @Override
    @SneakyThrows
    public void doGet(HttpServletRequest req, HttpServletResponse res) {
        var pathInfo = req.getPathInfo();

        if (pathInfo == null) {
            req.setAttribute("errorMessage", "Page does not exist");
            req.getRequestDispatcher("/WEB-INF/pages/error-page.jsp")
                    .forward(req, res);
            return;
        } else {
            pathInfo = StringUtils.substringAfterLast(pathInfo, "/");
        }

        if (StringUtils.equals(pathInfo, "success")) {
            req.getRequestDispatcher("/WEB-INF/pages/success-page.jsp")
                    .forward(req, res);
        } else {
            req.setAttribute("errorMessage", "Unknown error");
            req.getRequestDispatcher("/WEB-INF/pages/error-page.jsp")
                    .forward(req, res);
        }
    }

}
