package com.maksympanov.hneu.mjt.labfourth.lab4.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class UserInfoDto {

    private Long id;

    private String firstName;

    private String lastName;

    private String nickname;

    private String email;

    private String phoneNumber;

    private String sex;

    private LocalDate dateOfBirth;

    private String dateAdded;

}
