const switchEditMode = () => {
    const userInfoStyle = document.getElementById('user_info').style;
    const userUpdateFormStyle = document.getElementById('user_update_form').style;

    if (userInfoStyle.display === 'none') {
        userInfoStyle.display = 'block';
        userUpdateFormStyle.display = 'none';
    } else {
        userInfoStyle.display = 'none';
        userUpdateFormStyle.display = 'flex';
    }

};

const validate = () => {
    const defaultBorder = '1px grey solid';
    const defaultBgColor = 'transparent';

    const firstName = document.getElementById('firstName');
    if (firstName.value.length < 2) {
         const err = document.getElementById('firstNameErr');
         err.innerHTML = 'Should be at least 2 letters';
         firstName.style.border = '1px rgba(255, 0, 0, 0.5) solid';
         firstName.style.backgroundColor = 'rgba(255, 0, 0, 0.1)';
         return false;
    } else {
        document.getElementById('firstNameErr').innerHTML = '';
        firstName.style.border = defaultBorder;
        firstName.style.backgroundColor = defaultBgColor;
    }

    const nickname = document.getElementById('nickname');
    if (nickname.value.length < 1) {
        const err = document.getElementById('nicknameErr');
        err.innerHTML = 'Should not be empty';
        nickname.style.border = '1px rgba(255, 0, 0, 0.5) solid';
        nickname.style.backgroundColor = 'rgba(255, 0, 0, 0.1)';
        return false;
    } else {
        document.getElementById('nicknameErr').innerHTML = '';
        nickname.style.border = defaultBorder;
        nickname.style.backgroundColor = defaultBgColor;
    }

    const email = document.getElementById('email');
    const emailRegex = /(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
    if (email.value == null || email.value === '' || !emailRegex.test(email.value)) {
        const err = document.getElementById('emailErr');
        err.innerHTML = 'Should be a valid email';
        email.style.border = '1px rgba(255, 0, 0, 0.5) solid';
        email.style.backgroundColor = 'rgba(255, 0, 0, 0.1)';
        return false;
    } else {
        document.getElementById('emailErr').innerHTML = '';
        email.style.border = defaultBorder;
        email.style.backgroundColor = defaultBgColor;
    }

    const phoneNumber = document.getElementById('phoneNumber');
    const phoneNumberRegex = /(\+?38)?(0\d{2})\d{7}/;
    if (phoneNumber.value == null || phoneNumber.value === '' || !phoneNumberRegex.test(phoneNumber.value)) {
        const err = document.getElementById('phoneNumberErr');
        err.innerHTML = 'Should be a valid phone number';
        phoneNumber.style.border = '1px rgba(255, 0, 0, 0.5) solid';
        phoneNumber.style.backgroundColor = 'rgba(255, 0, 0, 0.1)';
        return false;
    } else {
        document.getElementById('phoneNumberErr').innerHTML = '';
        phoneNumber.style.border = defaultBorder;
        phoneNumber.style.backgroundColor = defaultBgColor;
    }

    const sex = document.getElementById('sex');
    if (sex.value == null || sex.value === '') {
        const err = document.getElementById('sexErr');
        err.innerHTML = 'Should be selected';
        sex.style.border = '1px rgba(255, 0, 0, 0.5) solid';
        sex.style.backgroundColor = 'rgba(255, 0, 0, 0.1)';
        return false;
    } else {
        document.getElementById('sexErr').innerHTML = '';
        sex.style.border = defaultBorder;
        sex.style.backgroundColor = defaultBgColor;
    }

    const dob = document.getElementById('dob');
    if (dob.value == null || dob.value === '') {
        const err = document.getElementById('dobErr');
        err.innerHTML = 'Should not be empty';
        dob.style.border = '1px rgba(255, 0, 0, 0.5) solid';
        dob.style.backgroundColor = 'rgba(255, 0, 0, 0.1)';
        return false;
    } else {
        document.getElementById('dobErr').innerHTML = '';
        dob.style.border = defaultBorder;
        dob.style.backgroundColor = defaultBgColor;
    }

    var selectedDob = new Date(dob);
    if (selectedDob < new Date(1920, 1, 1) || selectedDob > new Date()) {
        const err = document.getElementById('dobErr');
        err.innerHTML = 'Invalid date';
        dob.style.border = '1px rgba(255, 0, 0, 0.5) solid';
        dob.style.backgroundColor = 'rgba(255, 0, 0, 0.1)';
        return false;
    } else {
        document.getElementById('dobErr').innerHTML = '';
        dob.style.border = defaultBorder;
        dob.style.backgroundColor = defaultBgColor;
    }

    return true;
};

const cleanFields = () => {
    document.getElementById('firstName').value = '';
    document.getElementById('lastName').value = '';
    document.getElementById('nickname').value = '';
    document.getElementById('email').value = '';
    document.getElementById('phoneNumber').value = '';
    document.getElementById('sex').value = '';
    document.getElementById('dob').value = '';
};