<%@ taglib uri="jakarta.tags.core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>User Info</title>
    <link rel="stylesheet" href="../../styles/common.css" type="text/css" />
    <link rel="stylesheet" href="../../styles/header.css" type="text/css" />
</head>
<body>

<header>
    <nav>
        <ul>
            <li><a href="${pageContext.request.contextPath}/users">Users</a></li>
            <li><a href="${pageContext.request.contextPath}/registration">Add User</a></li>
        </ul>
    </nav>
</header>

<div class="content">

    <div id="user_info" class="data-box tall-box" style="padding: 3rem">
        <div class="header-box">
            <h1 class="centered-text">${user.firstName}'s Info</h1>
        </div>
        <br />
        <table style="padding: 1rem; font-size: 1.3rem">
            <tbody>
                <tr>
                    <td>First Name</td>
                    <td>${user.firstName}</td>
                </tr>
                <tr>
                    <td>Last Name</td>
                    <td>${user.lastName}</td>
                </tr>
                <tr>
                    <td>Nickname</td>
                    <td>${user.nickname}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>${user.email}</td>
                </tr>
                <tr>
                    <td>Phone Number</td>
                    <td>${user.phoneNumber}</td>
                </tr>
                <tr>
                    <td>Sex</td>
                    <td>${user.sex}</td>
                </tr>
                <tr>
                    <td>Date of Birth</td>
                    <td>${user.dateOfBirth}</td>
                </tr>
                <tr>
                    <td>Date Added</td>
                    <td>${user.dateAdded}</td>
                </tr>
            </tbody>
        </table>

        <button onclick="switchEditMode()" class="clean-button">To Edit Mode</button>
    </div>

    <div id="user_update_form" class="data-box tall-box" style="display: none;">
        <div class="header-box">
            <h1 class="centered-text">Change user info</h1>
            <p class="annotation">Required fields marked with *</p>
        </div>
        <br />
        <br />
        <br />
        <form class="centered-form" method="post" action="/users/${user.id}">
            <div class="labels-box">
                <label for="firstName">First Name<span style="font-size: 0.8rem">*</span></label><br />
            </div>
            <input id="firstName" name="firstName" type="text" value="${user.firstName}" required />

            <div class="labels-box">
                <label for="lastName">Last Name</label><br />
            </div>
            <input id="lastName" name="lastName" type="text" value="${user.lastName}" />

            <div class="labels-box">
                <label for="nickname">Nickname<span style="font-size: 0.8rem">*</span></label><br />
            </div>
            <input id="nickname" name="nickname" type="text" value="${user.nickname}" required />

            <div class="labels-box">
                <label for="email">Email<span style="font-size: 0.8rem">*</span></label><br />
            </div>
            <input id="email" name="email" type="email" value="${user.email}" required />

            <div class="labels-box">
                <label for="phoneNumber">Phone Number<span style="font-size: 0.8rem">*</span></label><br />
            </div>
            <input id="phoneNumber" name="phoneNumber" type="text" value="${user.phoneNumber}" required />

            <div class="labels-box">
                <label for="sex">Sex<span style="font-size: 0.8rem">*</span></label><br />
            </div>
            <select id="sex" name="sex" required>
                <option value="MALE" selected>Male</option>
                <option value="FEMALE">Female</option>
            </select>

            <div class="labels-box">
                <label for="dob">Date of Birth<span style="font-size: 0.8rem">*</span></label><br/>
            </div>
            <input id="dob" name="dob" type="date" value="${user.dateOfBirth}" required />

            <br />

            <input type="submit" value="Save" class="case-button" />

        </form>

        <button onclick="switchEditMode()" class="clean-button" style="width: 85%">To Readonly Mode</button>

    </div>
</div>

<script src="${pageContext.request.contextPath}/scripts/UserLogic.js"></script>

</body>
</html>
