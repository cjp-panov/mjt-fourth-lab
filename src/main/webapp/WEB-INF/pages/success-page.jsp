<%@ taglib uri="jakarta.tags.core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Success</title>
    <link rel="stylesheet" href="../../styles/common.css" type="text/css" />
    <link rel="stylesheet" href="../../styles/header.css" type="text/css" />
</head>
<body>

<header>
    <nav>
        <ul>
            <li><a href="${pageContext.request.contextPath}/users">Users</a></li>
            <li><a href="${pageContext.request.contextPath}/registration">Add User</a></li>
        </ul>
    </nav>
</header>

<div class="content">
    <div class="data-box centered">
        <div class="header-box">
            <h1 class="centered-text">Success!</h1>
            <p class="annotation centered-text">Operation has succeeded</p>
        </div>
        <form class="flex-form" action="${pageContext.request.contextPath}/users" method="get">
            <button class="case-button" type="submit">Return to Users list</button>
        </form>
    </div>
</div>

</body>
</html>