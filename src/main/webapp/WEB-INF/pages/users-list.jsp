<%@ taglib uri="jakarta.tags.core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Users</title>
    <link rel="stylesheet" href="../../styles/common.css" type="text/css" />
    <link rel="stylesheet" href="../../styles/header.css" type="text/css" />
</head>
<body>
<header>
    <nav>
        <ul>
            <li><a href="${pageContext.request.contextPath}/users">Users</a></li>
            <li><a href="${pageContext.request.contextPath}/registration">Add User</a></li>
        </ul>
    </nav>
</header>

<div class="content content-top">
    <div class="data-box huge-box centered-table-box">
        <div class="header-box">
            <h1 class="centered-text">Users List</h1>
            <p class="annotation centered-text">Number of records - ${usersDto.usersCount}</p>
        </div>
        <hr />
        <br />
        <br />
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Nickname</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Sex</th>
                    <th>Date of Birth</th>
                    <th>Date Added</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="user" items="${usersDto.users}">
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.firstName}</td>
                        <td>${user.lastName}</td>
                        <td>${user.nickname}</td>
                        <td>${user.email}</td>
                        <td>${user.phoneNumber}</td>
                        <td>${user.sex}</td>
                        <td>${user.dateOfBirth}</td>
                        <td>${user.dateAdded}</td>
                        <td>
                            <form action="/users/${user.id}" method="get">
                                <input type="submit" value="Open" class="edit-button" style="color: greenyellow" />
                            </form>
                        </td>
                        <td>
                            <form action="/users/delete/${user.id}" method="post">
                                <input type="submit" value="Delete" class="edit-button" style="color: red" />
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>
