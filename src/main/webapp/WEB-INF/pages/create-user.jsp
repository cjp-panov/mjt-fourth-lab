<%@ taglib uri="jakarta.tags.core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Create User</title>
    <link rel="stylesheet" href="../../styles/common.css" type="text/css" />
    <link rel="stylesheet" href="../../styles/header.css" type="text/css" />
</head>
<body>

<header>
    <nav>
        <ul>
            <li><a href="${pageContext.request.contextPath}/users">Users</a></li>
            <li><a href="${pageContext.request.contextPath}/registration">Add User</a></li>
        </ul>
    </nav>
</header>

<div class="content">
    <div class="data-box tall-box">
        <div class="header-box">
            <h1 class="centered-text">Create new user</h1>
            <p class="annotation">Required fields marked with *</p>
        </div>
        <br />
        <br />
        <br />
        <form id="create-form" class="centered-form" method="post" action="registration" onsubmit="return validate()">
            <div class="labels-box">
                <label for="firstName">First Name<span style="font-size: 0.8rem">*</span></label>
                <p id="firstNameErr" class="error"></p>
            </div>
            <input id="firstName" name="firstName" type="text" />

            <div class="labels-box">
                <label for="lastName">Last Name</label><br />
            </div>
            <input id="lastName" name="lastName" type="text" />

            <div class="labels-box">
                <label for="nickname">Nickname<span style="font-size: 0.8rem">*</span></label><br />
                <p id="nicknameErr" class="error"></p>
            </div>
            <input id="nickname" name="nickname" type="text" />

            <div class="labels-box">
                <label for="email">Email<span style="font-size: 0.8rem">*</span></label><br />
                <p id="emailErr" class="error"></p>
            </div>
            <input id="email" name="email" type="email" />

            <div class="labels-box">
                <label for="phoneNumber">Phone Number<span style="font-size: 0.8rem">*</span></label><br />
                <p id="phoneNumberErr" class="error"></p>
            </div>
            <input id="phoneNumber" name="phoneNumber" type="text" />

            <div class="labels-box">
                <label for="sex">Sex<span style="font-size: 0.8rem">*</span></label><br />
                <p id="sexErr" class="error"></p>
            </div>
            <select id="sex" name="sex">
                <option value="MALE" selected>Male</option>
                <option value="FEMALE">Female</option>
            </select>

            <div class="labels-box">
                <label for="dob">Date of Birth<span style="font-size: 0.8rem">*</span></label><br/>
                <p id="dobErr" class="error"></p>
            </div>
            <input id="dob" name="dob" type="date" />

            <br />

            <input type="submit" value="Create User" class="case-button" />

            <button onclick="cleanFields()" class="clean-button">Clean Form</button>
        </form>
    </div>
</div>

<script src="../../scripts/UserLogic.js"></script>

</body>
</html>
