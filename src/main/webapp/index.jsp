<%@ taglib uri="jakarta.tags.core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>User Registry</title>
    <link rel="stylesheet" href="./styles/common.css" type="text/css" />
    <link rel="stylesheet" href="../../styles/header.css" type="text/css" />
</head>
<body>

<div class="content">
    <div class="data-box">
        <div class="header-box">
            <h1 class="centered-text">User Registry Application</h1>
            <p class="annotation centered-text">By Maksym Panov</p>
        </div>
        <div class="buttons-box">
            <form class="flex-form" action="${pageContext.request.contextPath}/users" method="get">
                <button class="case-button" type="submit">See list of all users</button>
            </form>
            <form class="flex-form" action="${pageContext.request.contextPath}/registration" method="get">
                <button class="case-button" type="submit">Create new user record</button>
            </form>
        </div>
    </div>
</div>

</body>
</html>